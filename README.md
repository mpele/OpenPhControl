## Installing and Running

### Requirements

OpehPhControl runs on any system equipped with the Java Virtual Machine (1.7 or newer), which can be downloaded at no cost from [Oracle](http://www.oracle.com/technetwork/java/javase/downloads/index.html) or [OpenJDK](http://openjdk.java.net/).

### Installing and Running

OpehPhControl can be generated as an executable .jar file. Due to license limitation Marvin folder should be copied to same folder as .jar file.  
Try to double click the `jar` file or execute the following command:  
     `java -jar <path to jar>`

## Building OpehPhControl From Source

The OpenPhControl is build as Eclipse project, so it should be imported as Eclipse project and run as Java Aplication (OpenPhControlGUI).

## License

OpenPhControl is licensed under the [MIT license](https://tldrlegal.com/license/mit-license).
See the [LICENSE.md](LICENSE.md) for the full MIT license.

OpenPhControl also uses libraries distributed by other parties.

### Libraries

Project: jSerialComm  
URL:  http://fazecast.github.io/jSerialComm/  
License: GNU General Public License v3.0

Project: JFreeChart  
URL:     http://www.jfree.org/jfreechart/  
License: GNU Lesser General Public License (LGPL)

Project: Webcam Capture API  
URL:     http://webcam-capture.sarxos.pl/  
License: MIT License (MIT).

Project: Marvin  
URL:     http://marvinproject.sourceforge.net/en/index.html  
License: GNU Lesser General Public License (LGPL)


