package rs.mpele.pHStat.testing;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import rs.mpele.pHStat.GraphTimeFrame;
import rs.mpele.pHStat.Kontrolor;
import rs.mpele.pHStat.WorkingModeEnum;

import javax.swing.JButton;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import javax.swing.JCheckBox;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

public class TesterKontrolora extends JFrame {
	private static final long serialVersionUID = 1L;
	
	private JPanel contentPane;
	protected Kontrolor mKontrolor;
	OcitajPH_Simulator ocitajPH;
	private GraphTimeFrame grafikFrame;
	
	PhSimulatorFrame phFrame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TesterKontrolora frame = new TesterKontrolora();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	public TesterKontrolora() throws FileNotFoundException, IOException {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		setTitle("TesterKontrolora");
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		phFrame = new PhSimulatorFrame();
		phFrame.setVisible(true);

		mKontrolor = new Kontrolor(new ArduinoCoreTester());
		mKontrolor.SetTunings(1, 0, 0);
		mKontrolor.setCiljaniPH(10.55);
		mKontrolor.setZapreminaSuda(5);
		mKontrolor.setKoncentracijaNaOH(0.5);
		mKontrolor.setRucniRad(true);
		mKontrolor.getArduino().setKoeficijentPumpe(0.0009);
		mKontrolor.setVremeIzmedjuDvaDodavanja_s(10);

		mKontrolor.setWorkingMode(WorkingModeEnum.PH_STAT);	
//		mKontrolor.setWorkingMode(WorkingModeEnum.TITRATION);
		
		ocitajPH = new OcitajPH_Simulator();
		
		grafikFrame = new GraphTimeFrame();
		grafikFrame.setVisible(true);


		JButton btnNewButton = new JButton("New button");
		btnNewButton.addKeyListener(new KeyAdapter() {
			boolean pritisnuto = false;
//			private long vremePritiska;

			@Override
			public void keyPressed(KeyEvent e) {
				if(pritisnuto == false){					
//					System.out.println("--Pritisnuto*"+System.currentTimeMillis());
//					vremePritiska = System.currentTimeMillis();
					mKontrolor.rucnoDoziranjeUkljuci();
				}
				pritisnuto = true;
			}
			@Override
			public void keyReleased(KeyEvent e) {
//				System.out.println("--Pusteno*"+System.currentTimeMillis());
//				System.out.println("--"+(System.currentTimeMillis()-vremePritiska));
				mKontrolor.rucnoDoziranjeIskljuci();
				pritisnuto = false;
			}
		});

		contentPane.add(btnNewButton, BorderLayout.CENTER);
		
		final JCheckBox chckbxDaf = new JCheckBox("Rucno");
		chckbxDaf.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				mKontrolor.setRucniRad(chckbxDaf.isSelected());
			}
		});
		chckbxDaf.setSelected(true);
		contentPane.add(chckbxDaf, BorderLayout.SOUTH);
		
		final JSpinner spinner = new JSpinner();
		spinner.setModel(new SpinnerNumberModel(6.0, 0.0, 14.0, 0.1));
		contentPane.add(spinner, BorderLayout.NORTH);


		final long timeInterval = 1000;
		Runnable runnable = new Runnable() {
			public void run() {
				while (true) {
					//Double ph = ocitajPH.obradi();					
					try {
						mKontrolor.obradi(phFrame.obradi());
						//mKontrolor.addML(0.1);
						//grafikFrame.dodajVrednostiDodataZapremina(mKontrolor.getIzlazPodaci());
						grafikFrame.dodajVrednosti(mKontrolor.getIzlazPodaci());
						Thread.sleep(timeInterval);
					} catch (InterruptedException | ParseException e) {
						e.printStackTrace();
					}
				}
			}
		};

		Thread thread = new Thread(runnable);
		thread.start();
	}
	
	
	public static double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    BigDecimal bd = new BigDecimal(value);
	    bd = bd.setScale(places, RoundingMode.HALF_UP);
	    return bd.doubleValue();
	}
}


