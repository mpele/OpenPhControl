package rs.mpele.pHStat;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;
import rs.mpele.pHStat.ArduinoCore;
import rs.mpele.pHStat.Webcam.OcitajPH;
import rs.mpele.pHStat.Webcam.WebcamPanelPainter;

import javax.swing.JLabel;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFileChooser;

import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.WebcamPanel;
import com.github.sarxos.webcam.WebcamResolution;

import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.awt.event.ActionEvent;
import javax.swing.JRadioButton;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import javax.swing.border.TitledBorder;
import javax.swing.border.LineBorder;
import java.awt.Color;

import javax.swing.BoxLayout;
import java.awt.FlowLayout;
import java.awt.Dimension;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JTabbedPane;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

public class OpenPhControlGUI extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	protected ArduinoCore mArduinoCore;
	private WebcamPanel mPanel;
	protected Webcam mWebcam;
	private WebcamPanelPainter mPainter;
	private Kontrolor mKontrolor;
	private GraphTimeFrame mFrameGrafik;
	private GraphVolumeFrame mFrameGrafikVolume;
	private JLabel mPHLabel;
	private JLabel mVremeIzmedjDodavanjaLabel;
	private JLabel mFiksnoDodavanjeLabel;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					OpenPhControlGUI frame = new OpenPhControlGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	public OpenPhControlGUI() throws FileNotFoundException, IOException {

		mArduinoCore = new ArduinoCore();
		mKontrolor = new Kontrolor(mArduinoCore);

		mFrameGrafik = new GraphTimeFrame();
		mFrameGrafikVolume = new GraphVolumeFrame();


		//		protected ButtonGroup pumpGroup = new ButtonGroup();

		final ButtonGroup pumpGroup = new ButtonGroup();


		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 530, 500);
		setTitle("OpenPhControl");

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnPumpModules = new JMenu("Pump modules");
		menuBar.add(mnPumpModules);

		JMenuItem mntmAddNewPump = new JMenuItem("Add new pump");
		mntmAddNewPump.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				final ModuleControlPanel moduleControlPanel2 = new ModuleControlPanel( new ArduinoCore());
				contentPane.add(moduleControlPanel2, "cell 0 1,growx,aligny center");
				pumpGroup.add(moduleControlPanel2.getRadioButton());
				moduleControlPanel2.getRadioButton().addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						mArduinoCore = moduleControlPanel2.getArduino();
						mKontrolor.setArduino(mArduinoCore);
					}
				});
				revalidate();
				repaint();
			}
		});
		mnPumpModules.add(mntmAddNewPump);

		JMenuItem mntmDefineDescription = new JMenuItem("Define description");
		mntmDefineDescription.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ModulSettingsDialog dialog = new ModulSettingsDialog(mArduinoCore);
				dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
				dialog.setVisible(true);
			}
		});
		mnPumpModules.add(mntmDefineDescription);
		
		JSeparator separator2 = new JSeparator();
		mnPumpModules.add(separator2);
		
		JMenuItem mntmTestingPumpPrecision = new JMenuItem("Testing pump precision");
		mnPumpModules.add(mntmTestingPumpPrecision);
		mntmTestingPumpPrecision.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				TestingPumpPrecision frame = new TestingPumpPrecision(mArduinoCore);
				frame.setVisible(true);
			}
		});

		JMenu mnNewMenu = new JMenu("Experiment");
		menuBar.add(mnNewMenu);

		JMenuItem mntmNewMenuItem = new JMenuItem("New experiment");
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String s = JOptionPane.showInputDialog("Enter name of new experiment/file", mKontrolor.getNameOfSample());

				//If a string was returned, say so.
				if ((s != null) && (s.length() > 0)) {
					setTitle("OpenPhControl - "+s);
					mKontrolor.setNazivUzorka(s);
					return;
				}
			}
		});
		mnNewMenu.add(mntmNewMenuItem);

		JMenuItem menuItem = new JMenuItem("Graph - added volume");
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GraphTimeFrame grafik = new GraphTimeFrame();
				try {
					grafik.ucitajFajlDodateZapremine(mKontrolor.getNameOfSample());
				} catch (IOException | ParseException e1) {
					e1.printStackTrace();
				}
				grafik.setVisible(true);
			}
		});

		JMenuItem mntmNastaviEksperiment = new JMenuItem("Continue experiment");
		mntmNastaviEksperiment.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser fileChooser = new JFileChooser();
				File selectedFile;
				int result = fileChooser.showOpenDialog(null);
				if (result == JFileChooser.APPROVE_OPTION) {
					selectedFile = fileChooser.getSelectedFile();
					try {
						mFrameGrafik.ucitajFajl(selectedFile.getAbsolutePath());
					} catch (IOException | ParseException e) {
						e.printStackTrace();
					}
					System.out.println("Selected file: " + selectedFile.getAbsolutePath());



					// trazi prethodno dodatu zapreminu
					// TODO !!!! izmestiti ovo negde smislenije - i duplo se radi (u ucitavanju grafika)
					String lastLine = "";

					try (BufferedReader br = new BufferedReader(new FileReader(selectedFile.getAbsolutePath()))) {
						String line;
						while ((line = br.readLine()) != null) {
							lastLine = line;
						}
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
					String[] vrednosti = lastLine.split(",");
					System.out.println("Totally added volume "+vrednosti[4]);
					System.out.println("Experiment "+selectedFile.getName());

					mKontrolor.setUkupnoDodataZapremina(Double.valueOf(vrednosti[4]));
					mKontrolor.setNazivUzorka(selectedFile.getName());

				}
			}
		});
		mnNewMenu.add(mntmNastaviEksperiment);
		mnNewMenu.add(menuItem);

		JMenuItem mntmPh = new JMenuItem("Load graph pH/volume");
		mntmPh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser fileChooser = new JFileChooser();
				fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
				int result = fileChooser.showOpenDialog(null);
				if (result == JFileChooser.APPROVE_OPTION) {
					File selectedFile = fileChooser.getSelectedFile();
					GraphTimeFrame tmpGrafik = new GraphTimeFrame();
					try {
						tmpGrafik.ucitajFajl(selectedFile.getAbsolutePath());
					} catch (IOException | ParseException e) {
						e.printStackTrace();
					}
					tmpGrafik.setNaslov(selectedFile.getName());
					tmpGrafik.setVisible(true);
					System.out.println("Selected file: " + selectedFile.getAbsolutePath());
				}
			}
		});

		JSeparator separator = new JSeparator();
		mnNewMenu.add(separator);
		mnNewMenu.add(mntmPh);

		JMenuItem menuItem_1 = new JMenuItem("Load graph totally added volume");
		menuItem_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser fileChooser = new JFileChooser();
				fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
				int result = fileChooser.showOpenDialog(null);
				if (result == JFileChooser.APPROVE_OPTION) {
					File selectedFile = fileChooser.getSelectedFile();
					GraphTimeFrame tmpGrafik = new GraphTimeFrame();
					try {
						tmpGrafik.ucitajFajlDodateZapremine(selectedFile.getAbsolutePath());
					} catch (IOException | ParseException e) {
						e.printStackTrace();
					}
					tmpGrafik.setNaslov(selectedFile.getName());
					tmpGrafik.setVisible(true);
					System.out.println("Selected file: " + selectedFile.getAbsolutePath());
				}
			}
		});
		mnNewMenu.add(menuItem_1);
		contentPane = new JPanel();
		setContentPane(contentPane);
		contentPane.setLayout(new MigLayout("", "[grow]", "[][][][][][][]"));


		final ModuleControlPanel moduleControlPanel = new ModuleControlPanel(mArduinoCore);
		contentPane.add(moduleControlPanel, "cell 0 0,growx,aligny center");
		moduleControlPanel.getRadioButton().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				mArduinoCore = moduleControlPanel.getArduino();
				mKontrolor.setArduino(mArduinoCore);
			}
		});

		mArduinoCore = moduleControlPanel.getArduino();
		moduleControlPanel.getRadioButton().setSelected(true);
		pumpGroup.add(moduleControlPanel.getRadioButton());

		JButton btnNewButton = new JButton("on");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.out.println("on");
				mArduinoCore.komanda("A");
			}
		});


		contentPane.add(btnNewButton, "flowx,cell 0 2");

		JButton btnNewButton_1 = new JButton("off");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("off");
				mArduinoCore.komanda("B");
			}
		});
		contentPane.add(btnNewButton_1, "cell 0 2");


		// TODO Samo za lakse pronalazenje a za koriscenje Design-a
		if(Webcam.getWebcams().size()>0){
			// ukljucuje kameru i prikazuje snimak
			mWebcam = Webcam.getWebcams().get(Webcam.getWebcams().size()-1); // prikazuje poslednju kameru u nizu (eksternu ako je ima)
			//mWebcam = Webcam.getWebcams().get(0); // ovde se bira koja kamera

			mWebcam.setViewSize(WebcamResolution.VGA.getSize());


			mPanel = new WebcamPanel(mWebcam);
			mPanel.setFPSDisplayed(true);
			//mPanel.setDisplayDebugInfo(true);
			mPanel.setImageSizeDisplayed(true);

			mPanel.setFPSLimit(5.);

			mPainter = new WebcamPanelPainter(new OcitajPH(), mKontrolor, mFrameGrafik, mFrameGrafikVolume);
			mPanel.setPainter(mPainter);

			contentPane.add(mPanel, "cell 0 3,grow");

			//////////////////////////////////////////////////////////////////////////////////////	
		}

		final JButton btnUkljuciCitanje = new JButton("Trun on reading");
		btnUkljuciCitanje.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				mPainter.setPodesavanja();
				mPainter.setRadiCitanje();
				btnUkljuciCitanje.setBackground(Color.green);
			}
		});

		contentPane.add(btnUkljuciCitanje, "flowx,cell 0 4");

		JButton btnKalibracijaKamere = new JButton("Calibration of camera");
		btnKalibracijaKamere.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				mPainter.ukljuciKalibracijuKamere();
			}
		});
		contentPane.add(btnKalibracijaKamere, "cell 0 4");

		JButton btnKalibracijaPumpe = new JButton("Pump calibration");
		btnKalibracijaPumpe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				PumpCalibrationFrame frame = new PumpCalibrationFrame(mArduinoCore);
				frame.setVisible(true);
			}
		});
		contentPane.add(btnKalibracijaPumpe, "cell 0 2");


		ButtonGroup group = new ButtonGroup();

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		contentPane.add(tabbedPane, "cell 0 6,grow");



		final JPanel panelAutomatskiRad = new JPanel();
		tabbedPane.addTab("pH stat", null, panelAutomatskiRad, null);
		panelAutomatskiRad.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), "pH stat", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(51, 51, 51)));
		panelAutomatskiRad.setLayout(new BoxLayout(panelAutomatskiRad, BoxLayout.Y_AXIS));

		JPanel panel = new JPanel();
		panelAutomatskiRad.add(panel);
		panel.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));

		final JRadioButton automaticControllRButton = new JRadioButton("Automatic dosing");
		panel.add(automaticControllRButton);
		automaticControllRButton.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				mKontrolor.setRucniRad(!automaticControllRButton.isSelected());
			}
		});

		automaticControllRButton.setSelected(true);
		group.add(automaticControllRButton);

		JRadioButton manualDosingRButton = new JRadioButton("Manual dosing");
		panel.add(manualDosingRButton);
		group.add(manualDosingRButton);


		JButton btnDodavanjeRucno = new JButton("Dosing");
		panel.add(btnDodavanjeRucno);
		btnDodavanjeRucno.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnDodavanjeRucno.addKeyListener(new KeyAdapter() {
			boolean pritisnuto = false;
			//			private long vremePritiska;

			@Override
			public void keyPressed(KeyEvent e) {
				if(pritisnuto == false){					
					//				System.out.println("--Pritisnuto*"+System.currentTimeMillis());
					//				vremePritiska = System.currentTimeMillis();
					mKontrolor.rucnoDoziranjeUkljuci();
				}
				pritisnuto = true;
			}
			@Override
			public void keyReleased(KeyEvent e) {
				//				System.out.println("--Pusteno*"+System.currentTimeMillis());
				//				System.out.println("--"+(System.currentTimeMillis()-vremePritiska));
				mKontrolor.rucnoDoziranjeIskljuci();
				pritisnuto = false;
			}
		});

		JPanel panel_4 = new JPanel();
		FlowLayout flowLayout_3 = (FlowLayout) panel_4.getLayout();
		flowLayout_3.setAlignment(FlowLayout.LEADING);
		panelAutomatskiRad.add(panel_4);


		final JButton btnUkljuciKontrolu = new JButton("Turn on control");
		panel_4.add(btnUkljuciKontrolu);
		btnUkljuciKontrolu.setBackground(Color.RED);
		btnUkljuciKontrolu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(mPainter.daLiJeUkljucenaKontrola()){
					btnUkljuciKontrolu.setBackground(Color.RED);
					mPainter.iskljuciKontrolu();
				}
				else{
					mKontrolor.setWorkingMode(WorkingModeEnum.PH_STAT);
					btnUkljuciKontrolu.setBackground(Color.GREEN);
					mPainter.ukljuciKontrolu();
				}
			}
		});


		final JCheckBox checkBoxAutomatskoPodesavanjeParametara = new JCheckBox("Automatic tuning parameters");
		panel_4.add(checkBoxAutomatskoPodesavanjeParametara);
		checkBoxAutomatskoPodesavanjeParametara.setSelected(true);
		checkBoxAutomatskoPodesavanjeParametara.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				mKontrolor.SetTunings(0.1, 0, 0);
				mKontrolor.setCiljaniPH(10.55);
				mKontrolor.setZapreminaSuda(5);
				mKontrolor.setKoncentracijaNaOH(0.5);
				mKontrolor.setAutomatskoPodesavanjeParametara(checkBoxAutomatskoPodesavanjeParametara.isSelected());
			}
		});

		JPanel panel_1 = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel_1.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		panelAutomatskiRad.add(panel_1);

		JLabel lblPh = new JLabel("Target pH:");
		panel_1.add(lblPh);

		mPHLabel = new JLabel(String.valueOf(mKontrolor.getCiljaniPh()));
		mPHLabel.setMinimumSize(new Dimension(30, 15));
		mPHLabel.setMaximumSize(new Dimension(30, 15));
		panel_1.add(mPHLabel);

		JButton button_2 = new JButton("Set");
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				CiljaniPhDialog dialog = new CiljaniPhDialog(mKontrolor.getCiljaniPh());
				dialog.setVisible(true);
				if(dialog.bOK == true){
					mKontrolor.setCiljaniPH(dialog.getVrednostPh());
					mPHLabel.setText(mKontrolor.getCiljaniPh().toString());
				}
			}
		});
		panel_1.add(button_2);

		JPanel panel_2 = new JPanel();
		FlowLayout flowLayout_1 = (FlowLayout) panel_2.getLayout();
		flowLayout_1.setAlignment(FlowLayout.LEFT);
		panelAutomatskiRad.add(panel_2);

		JLabel lblVremeIzmedjuDodavanja = new JLabel("Minimal time between two dosings, s: ");
		panel_2.add(lblVremeIzmedjuDodavanja);

		mVremeIzmedjDodavanjaLabel = new JLabel(String.valueOf(mKontrolor.getMinVremeIzmedjuDvaDodavanja_s()));
		mVremeIzmedjDodavanjaLabel.setMinimumSize(new Dimension(30, 15));
		mVremeIzmedjDodavanjaLabel.setMaximumSize(new Dimension(30, 15));
		panel_2.add(mVremeIzmedjDodavanjaLabel);

		JButton button = new JButton("Set");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MinVremeIzmedjuDodavanjaDialog dialog = new MinVremeIzmedjuDodavanjaDialog(mKontrolor.getMinVremeIzmedjuDvaDodavanja_s());
				dialog.setVisible(true);
				if(dialog.bOK == true){
					mKontrolor.setVremeIzmedjuDvaDodavanja_s(dialog.getMinIzmedjuDvaDodavanja());
					mVremeIzmedjDodavanjaLabel.setText(mKontrolor.getMinVremeIzmedjuDvaDodavanja_s().toString());
				}

			}
		});
		panel_2.add(button);

		JPanel panel_3 = new JPanel();
		FlowLayout flowLayout_2 = (FlowLayout) panel_3.getLayout();
		flowLayout_2.setAlignment(FlowLayout.LEFT);
		panelAutomatskiRad.add(panel_3);

		JLabel lblFiksnoDodavanjeMl = new JLabel("Dosing volume, ml:");
		panel_3.add(lblFiksnoDodavanjeMl);

		mFiksnoDodavanjeLabel = new JLabel(String.valueOf(mKontrolor.getJedinicnoFiksnoDodavanje_mL()));
		mFiksnoDodavanjeLabel.setMinimumSize(new Dimension(30, 15));
		mFiksnoDodavanjeLabel.setMaximumSize(new Dimension(30, 15));
		panel_3.add(mFiksnoDodavanjeLabel);

		JButton button_1 = new JButton("Set");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FiksnoDodavanjeDialog dialog = new FiksnoDodavanjeDialog(mKontrolor.getJedinicnoFiksnoDodavanje_mL());
				dialog.setVisible(true);
				if(dialog.bOK == true){
					mKontrolor.setJedinicnoFiksnoDodavanje_mL(dialog.getZapreminaFiksnogDodavanja());
					mFiksnoDodavanjeLabel.setText(mKontrolor.getJedinicnoFiksnoDodavanje_mL().toString());
				}

			}
		});
		panel_3.add(button_1);

		//Salje kontroloru labele koje treba da menja u slucaju promene parametara rada
		mKontrolor.definisiLabeleZaAzuriranjeParametara(mFiksnoDodavanjeLabel, mVremeIzmedjDodavanjaLabel);

		JPanel panelTitration = new JPanel();
		tabbedPane.addTab("Titration", null, panelTitration, null);
		panelTitration.setBorder(new TitledBorder(null, "Titration", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelTitration.setLayout(new BoxLayout(panelTitration, BoxLayout.Y_AXIS));

		JPanel panel_6 = new JPanel();
		panel_6.setMaximumSize(new Dimension(32767, 10));
		FlowLayout flowLayout_5 = (FlowLayout) panel_6.getLayout();
		flowLayout_5.setAlignment(FlowLayout.LEFT);
		panelTitration.add(panel_6);

		final JButton btnStartTitration = new JButton("Start titration");
		btnStartTitration.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(mPainter.daLiJeUkljucenaKontrola()){
					btnStartTitration.setBackground(Color.RED);
					mPainter.iskljuciKontrolu();
				}
				else{
					mKontrolor.setWorkingMode(WorkingModeEnum.TITRATION);
					btnStartTitration.setBackground(Color.GREEN);
					mPainter.ukljuciKontrolu();
				}		
			}
		});
		panel_6.add(btnStartTitration);

		JPanel panel_5 = new JPanel();
		FlowLayout flowLayout_4 = (FlowLayout) panel_5.getLayout();
		flowLayout_4.setAlignment(FlowLayout.LEFT);
		panelTitration.add(panel_5);

		JLabel lblAddingDose = new JLabel("Adding dose");
		panel_5.add(lblAddingDose);
		
		final JSpinner spinner = new JSpinner();
		spinner.setModel(new SpinnerNumberModel(new Double(0.5), null, null, new Double(0.1)));
		spinner.setPreferredSize(new Dimension(60, 20));
		panel_5.add(spinner);
		
		JPanel panel_7 = new JPanel();
		panel_5.add(panel_7);

		JButton btnSet = new JButton("Set");
		btnSet.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				mKontrolor.setTitrtionDose((Double) spinner.getValue());
			}
		});
		panel_7.add(btnSet);

		JButton button_4 = new JButton("Graph");
		button_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				mFrameGrafik.setVisible(true);
				mFrameGrafikVolume.setVisible(true);
			}
		});
		contentPane.add(button_4, "flowx,cell 0 4");

		JButton btnRestetGrafika = new JButton("Reset graph");
		btnRestetGrafika.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mFrameGrafik.resetPrikaza();
			}
		});
		contentPane.add(btnRestetGrafika, "cell 0 4");	
	}

}
