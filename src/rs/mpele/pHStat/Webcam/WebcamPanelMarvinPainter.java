package rs.mpele.pHStat.Webcam;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import com.github.sarxos.webcam.WebcamPanel;


public class WebcamPanelMarvinPainter implements WebcamPanel.Painter{


	public void paintImage(WebcamPanel panel, BufferedImage image, Graphics2D g2) {
		Graphics2D g = image.createGraphics();

		int fontSize = 90;

	    g.setFont(new Font("TimesRoman", Font.PLAIN, fontSize));
		
		g.setColor(Color.WHITE);
		g.drawString("Ura !!!", 300, 300);
		
		g.setColor(Color.GREEN);
		g.drawRect(300 - 50, 300 - 5, 100, 10);
		
		g.dispose();

		panel.getDefaultPainter().paintImage(panel, image, g2);
	}


	public void paintPanel(WebcamPanel panel, Graphics2D g2) {
		panel.getDefaultPainter().paintPanel(panel, g2);
	}

}
