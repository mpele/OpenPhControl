package rs.mpele.pHStat;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Dimension;

public class FiksnoDodavanjeDialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JSpinner spinner;
	boolean bOK = false;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			FiksnoDodavanjeDialog dialog = new FiksnoDodavanjeDialog(0.5);
			dialog.setVisible(true);
			if(dialog.bOK == true)
				System.out.println(dialog.getZapreminaFiksnogDodavanja());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	Double getZapreminaFiksnogDodavanja() {
		return (Double) spinner.getValue();
	}

	/**
	 * Create the dialog.
	 */
	public FiksnoDodavanjeDialog(double pHstari) {
		setTitle("Definisanje zapremine fiksnog dodavanja");
		setBounds(100, 100, 314, 104);
		setModal(true);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);;
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setLayout(new FlowLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						bOK = true;
						dispose();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						setVisible(false);
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
		{
			JLabel lblNewLabel = new JLabel("Fiksno dodavanje, ml:");
			getContentPane().add(lblNewLabel, BorderLayout.NORTH);
		}
		{
			double min = 0.0;
			double max = 14.0;
			double step = 0.1;
			spinner =new JSpinner(new SpinnerNumberModel(pHstari, min, max, step));
			spinner.setMinimumSize(new Dimension(150, 20));
			spinner.setPreferredSize(new Dimension(150, 20));
			getContentPane().add(spinner, BorderLayout.WEST);
		}
	}

}
